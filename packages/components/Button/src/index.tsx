import React from "react";

function Button({ buttonLabel = "Click me!" }) {
  return <button>{buttonLabel}</button>;
}

export default Button;
